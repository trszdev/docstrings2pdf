docstring2pdf
=============

docstrings2pdf converts multiple python3 source files to pdf document. This package requires python3.6+.

### usage
```bash
python -m docstrings2pdf *.py -o out.pdf
```

### installation
```bash
python setup.py
```

### tests
```bash
python run_tests.py
```

### markdowns
* [reST](https://docs.python.org/3.1/documenting/rest.html)
* [numpy](https://github.com/numpy/numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt#docstring-standard)
* [Google](http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html)


Already tested on "Python 3.6.2 (v3.6.2:5fd33b5, Jul  8 2017, 04:57:36) [MSC v.1900 64 bit (AMD64)] on win32"