from docstrings2pdf.typing import SourceFile, SourceNodeType, SourceNode
from typing import Iterable
from reportlab.platypus import Paragraph, SimpleDocTemplate, PageBreak
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.enums import TA_RIGHT
from reportlab.lib.colors import HexColor
from html import escape
from docstrings2pdf.docstrings import parse_docstring
from re import sub


class PdfBuilder:
    def __init__(self):
        self.sf_style = ParagraphStyle('h1', alignment=TA_RIGHT, fontSize=10,
                                       fontName='Times-Italic', borderRadius=3, spaceAfter=10,
                                       borderPadding=5)
        self.node_style = ParagraphStyle('h2', backColor=HexColor('#eaeaea'), borderPadding=5,
                                         fontSize=12, fontName='Courier-Bold', spaceAfter=8)
        self.doc_style = ParagraphStyle('h3', fontSize=10, fontName='Helvetica',
                                        borderPadding=3, leftIndent=30, spaceAfter=30,
                                        backColor=HexColor('#ffffff'), borderWidth=1, borderColor=HexColor('#eaeaea'))
        self.page_break = PageBreak()
        self.contents = []

    def add_node(self, node_text: str) -> None:
        self.contents.append(Paragraph(node_text, self.node_style))

    def add_doc(self, doc_text: str) -> None:
        self.contents.append(Paragraph(doc_text, self.doc_style))

    def add_page_break(self) -> None:
        self.contents.append(self.page_break)

    def add_sf(self, sf_text: str) -> None:
        self.contents.append(Paragraph(sf_text, self.sf_style))

    def save(self, filename: str) -> None:
        SimpleDocTemplate(filename).build(self.contents)


def escape_all(text: str) -> str:
    text = sub(r'\\(.)', r'\1', text)
    return escape(text)


def text2html(text: str) -> Iterable[str]:
    result = escape_all(text).splitlines()
    return [sub(r'``(.+?)``|:[a-z]+:`(.+?)`', r'<i>\1\2</i>', x) for x in result]


def create_docstring(node_doc: str) -> str:
    parsed = parse_docstring(node_doc or '...')
    text = []
    if parsed.returns:
        text.append('<b><u>Returns:</u></b>')
        text.extend(text2html(parsed.returns.description or parsed.returns.type))
    if parsed.args:
        text.append('<b><u>Parameters:</u></b>')
    for arg in parsed.args:
        text.append(f'+ <b><i>{escape_all(arg.name)}'
                    f'{": " + escape_all(arg.type) if arg.type else ""}</i></b>')
        text.extend(text2html(arg.description))
    if parsed.args:
        text.append('')

    text.append('<b><u>Description:</u></b>')
    text.extend(text2html(parsed.text))
    return '<br/>'.join(text)


def render_node(node: SourceNode, pdf: PdfBuilder, skip_blanks: bool, path='') -> None:
    if not skip_blanks or (skip_blanks and node.docstring):
        node_type = "class" if node.type == SourceNodeType.CLASS else "def"
        node_args = f'({", ".join(node.args)})'
        if node.args and node.args[-1].startswith('->'):
            node_args = f'({", ".join(node.args[:-1])}) {node.args[-1]}'
        pdf.add_node(f'{node_type} {path}{escape(node.name)}{node_args}:')
        pdf.add_doc(create_docstring(node.docstring))
    for child in node.children:
        render_node(child, pdf, skip_blanks, f'{path}{escape(node.name)}.')


def has_content(node: SourceNode) -> bool:
    if node.docstring:
        return True
    return any(map(has_content, node.children))


def render(pdf_filename: str, source_files: Iterable[SourceFile], skip_blanks: bool) -> None:
    pdf = PdfBuilder()
    for sf in source_files:
        if not any(map(has_content, sf.nodes)) and skip_blanks:
            continue
        pdf.add_sf(sf.name)
        for node in sf.nodes:
            render_node(node, pdf, skip_blanks)
        pdf.add_page_break()
    pdf.save(pdf_filename)
