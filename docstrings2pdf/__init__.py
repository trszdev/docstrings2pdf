from docstrings2pdf.parse import parse_file
from docstrings2pdf.pdf import render
from docstrings2pdf.typing import SourceFile, FormatType
from typing import Union
from argparse import ArgumentParser
from glob import glob
from itertools import chain
from traceback import print_exc


def parse_file2(filename: str) -> Union[None, SourceFile]:
    try:
        result = parse_file(filename)
        return result if result.nodes else None
    except:
        print_exc()
        return None


def get_format_type(format_str: str) -> FormatType:
    return FormatType({
        "none": 0,
        "highlight": 1,
        "format": 2
    }[format_str])


def main(args):
    parser = ArgumentParser(usage='python -m docstrings2pdf *.py -so out.pdf')
    parser.add_argument('globs', metavar='GLOB', nargs='+',
                        help='glob masks for python source files')
    parser.add_argument('-o', '--output', help='pdf filename', required=True)
    parser.add_argument('-s', '--skip-blanks', default=False, action='store_true',
                        help='skip files without docstrings or with errors')
    parsed = parser.parse_args(args)
    files = set(chain(*map(glob, parsed.globs)))
    files = filter(bool, map(parse_file2, files))
    render(parsed.output, files, parsed.skip_blanks)