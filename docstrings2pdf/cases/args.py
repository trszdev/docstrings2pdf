def simple_args(first, second=None):
    pass


def annotated(first: str, second: int = 0) -> float:
    pass


def kwargs(first, second, *args, **kwargs):
    pass


def named_only(first, *, named1, named2):
    pass


def nonconstant_arg(first=named_only):
    pass


class A:
    pass


class B(A, object):
    pass
