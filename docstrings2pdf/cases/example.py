class Klass:
    """
        Klass description
    """
    def __init__(self):
        pass

    def method1(self, arg):
        """
        does nothing
        :param arg: arg is not used
        :return:
        """
        return 1+1

    def method2(self, q):
        """ One line docstring """
        pass


def IDONTLIKEpep8():
    """ realy? """
    pass


def malicious_docstring():
    """ <script>alert(1)</script> """
    pass


def no_doc_at_all(a, b, c, d):
    return a*b*c*d