from typing import Tuple, List
from enum import IntEnum, auto


class SourceNodeType(IntEnum):
    FUNCTION = auto()
    CLASS = auto()


class SourceNode:
    def __init__(self, _type: SourceNodeType, children: Tuple['SourceNode'],
                 name: str, docstring: str = None, args: Tuple[str] = tuple()):
        self.type = _type
        self.children = children
        self.docstring = docstring
        self.name = name
        self.args = args


class SourceFile:
    def __init__(self, nodes: Tuple[SourceNode], name: str = None):
        self.name = name or 'undefined.py'
        self.nodes = nodes


class ParseError(Exception):
    pass


class FormatType(IntEnum):
    NONE = auto()
    HIGHLIGHT = auto()
    FORMAT = auto()


class FunctionArg:
    def __init__(self, name: str, _type: str, description: str):
        self.name = name
        self.description = description
        self.type = _type


class ParsedDocstring:
    def __init__(self, text: str, returns: FunctionArg, args: Tuple[FunctionArg]):
        self.text = text
        self.returns = returns
        self.args = args
