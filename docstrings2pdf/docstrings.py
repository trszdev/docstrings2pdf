from docstrings2pdf.typing import ParsedDocstring, FunctionArg
from sphinxcontrib.napoleon import Config
from sphinxcontrib.napoleon.docstring import GoogleDocstring, NumpyDocstring
from re import match
from textwrap import dedent
from collections import defaultdict


def get_indent(line):
    stripped = line.lstrip()
    count = len(line) - len(stripped)
    return line[:count]


def dedent2(text):
    lines = text.splitlines()
    for line in lines[1:]:
        indent = get_indent(line)
        if indent != line:
            lines[0] = indent + lines[0]
            break
    return dedent('\n'.join(lines))


def normalize_docstring(generic_docstring: str) -> str:
    ds = dedent(generic_docstring)
    numpy_ds = str(NumpyDocstring(ds, Config()))
    if numpy_ds != ds:
        return numpy_ds
    google_ds = str(GoogleDocstring(ds, Config()))
    if google_ds != ds:
        return google_ds
    return ds


def _parse_rst(rst):
    rst = dedent2(rst)
    tag, name, taglines = '', '', []
    tags = {x: defaultdict(list) for x in ['param', 'type', 'rtype', 'return', '']}
    for line in rst.splitlines():
        if get_indent(line):
            taglines.append(line)
            continue
        m = match(r'\s*(:(param|type|rtype|return) ?(.*?):)', line)
        if m:
            _, ntag, nname = m.groups()
            tags[tag][name] += taglines
            tag, name, taglines = ntag, nname, [line[len(m.group(0)):].strip()]
        else:
            tags[tag][name] += taglines
            tag, name, taglines = '', '', [line]
    tags[tag][name] += taglines
    return tags


def parse_rst(rst_doc: str) -> ParsedDocstring:
    parsed = _parse_rst(rst_doc)

    def get_text(tag, name):
        return '\n'.join(parsed[tag][name])
    rdesc = get_text('return', '')
    rtype = get_text('rtype', '')
    returns = FunctionArg('returns', rtype, rdesc) if rdesc or rtype else None
    args = tuple(FunctionArg(x, get_text('type', x), get_text('param', x))
                 for x in parsed['param'])
    return ParsedDocstring(get_text('', ''), returns, args)


def parse_docstring(docstring: str) -> ParsedDocstring:
    return parse_rst(normalize_docstring(docstring))