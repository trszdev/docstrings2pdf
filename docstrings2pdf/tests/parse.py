from docstrings2pdf.parse import parse_file
from docstrings2pdf.typing import SourceNodeType, ParseError
from assertpy import assert_that
from parameterized import parameterized


def assert_nodes(nodes):
    for node, name, children_amount, node_type, docstring, args in nodes:
        assert_that(node.name).is_equal_to(name)
        assert_that(node.children).is_length(children_amount)
        assert_that(node.type).is_equal_to(node_type)
        if docstring:
            assert_that(node.docstring).contains(docstring)
        else:
            assert_that(node.docstring).is_none()
        assert_that(node.args).is_equal_to(args)


def assert_sf(sf, name, nodes_amount):
    assert_that(sf.name).is_equal_to(name)
    assert_that(sf.nodes).is_length(nodes_amount)


def test_parse_file_example():
    sf = parse_file('../cases/example.py')
    klass, pep8, malicious, no_doc = sf.nodes
    init, method1, method2 = klass.children
    expected = [
        (klass, 'Klass', 3, SourceNodeType.CLASS, 'Klass description', ()),
        (init, '__init__', 0, SourceNodeType.FUNCTION, None, ('self', )),
        (method1, 'method1', 0, SourceNodeType.FUNCTION, 'does nothing', ('self', 'arg')),
        (method2, 'method2', 0, SourceNodeType.FUNCTION, 'One line docstring', ('self', 'q')),
        (pep8, 'IDONTLIKEpep8', 0, SourceNodeType.FUNCTION, 'realy?', ()),
        (malicious, 'malicious_docstring', 0, SourceNodeType.FUNCTION, '<script>alert(1)</script>', ()),
        (no_doc, 'no_doc_at_all', 0, SourceNodeType.FUNCTION, None, ('a', 'b', 'c', 'd')),
    ]
    assert_sf(sf, 'example.py', 4)
    assert_nodes(expected)


def test_parse_file_nested_class():
    sf = parse_file('../cases/nested_class.py')
    albert = sf.nodes[0]
    ben = albert.children[0]
    catalina = ben.children[0]
    david = catalina.children[0]
    expected = [
        (albert, 'Albert', 1, SourceNodeType.CLASS, None, ()),
        (ben, 'Ben', 1, SourceNodeType.CLASS, None, ()),
        (catalina, 'Catalina', 1, SourceNodeType.CLASS, 'Im catalina!', ()),
        (david, 'David', 0, SourceNodeType.CLASS, None, ()),
    ]
    assert_sf(sf, 'nested_class.py', 1)
    assert_nodes(expected)


def test_parse_file_nested_func():
    sf = parse_file('../cases/nested_func.py')
    red = sf.nodes[0]
    orange = red.children[0]
    yellow = orange.children[0]
    green, blue = yellow.children
    expected = [
        (red, 'red', 1, SourceNodeType.FUNCTION, None, ()),
        (orange, 'orange', 1, SourceNodeType.FUNCTION, 'HEY IM ORANGE', ()),
        (yellow, 'yellow', 2, SourceNodeType.FUNCTION, None, ()),
        (green, 'green', 0, SourceNodeType.FUNCTION, None, ()),
        (blue, 'blue', 0, SourceNodeType.FUNCTION, "'BLU'", ()),
    ]
    assert_sf(sf, 'nested_func.py', 1)
    assert_nodes(expected)


def test_parse_file_args():
    sf = parse_file('../cases/args.py')
    simple, annotated, kwargs, named_only, nonconstant_arg, a, b = sf.nodes
    expected = [
        (simple, 'simple_args', 0, SourceNodeType.FUNCTION, None, ('first', 'second=None')),
        (annotated, 'annotated', 0, SourceNodeType.FUNCTION, None, ('first: str', 'second: int=0', '-> float')),
        (kwargs, 'kwargs', 0, SourceNodeType.FUNCTION, None, ('first', 'second', '*args', '**kwargs')),
        (named_only, 'named_only', 0, SourceNodeType.FUNCTION, None, ('first', '*', 'named1', 'named2')),
        (nonconstant_arg, 'nonconstant_arg', 0, SourceNodeType.FUNCTION, None, ('first=named_only',)),
        (a, 'A', 0, SourceNodeType.CLASS, None, ()),
        (b, 'B', 0, SourceNodeType.CLASS, None, ('A', 'object',)),
    ]
    assert_sf(sf, 'args.py', 7)
    assert_nodes(expected)


@parameterized.expand([
    '../cases/wrong.py',
    '../cases/python2.py'
])
def test_parse_file_raises(filename):
    assert_that(lambda: parse_file(filename)).raises(ParseError)
