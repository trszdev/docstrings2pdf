from assertpy import assert_that
from docstrings2pdf import main
from shutil import rmtree
from pytest import fixture


@fixture
def clear():
    rmtree('rs.pdf', ignore_errors=True)
    rmtree('r.pdf', ignore_errors=True)
    rmtree('api.pdf', ignore_errors=True)


def test_request_library_skip_blanks(clear):
    main(['../cases/requests/*.py', '-so', 'rs.pdf'])
    assert_that('rs.pdf').is_file()


def test_request_library(clear):
    main(['../cases/requests/*.py', '-o', 'r.pdf'])
    assert_that('r.pdf').is_file()


def test_requests_api(clear):
    main(['../cases/requests/api.py', '-o', 'api.pdf'])
    assert_that('api.pdf').is_file()
