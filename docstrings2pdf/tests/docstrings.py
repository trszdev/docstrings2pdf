from docstrings2pdf.docstrings import parse_docstring
from docstrings2pdf.typing import FunctionArg
from assertpy import assert_that
from parameterized import parameterized


def assert_equals(actual: FunctionArg, expected: FunctionArg):
    assert_that(actual.name).is_equal_to(expected.name)
    assert_that(actual.description).is_equal_to(expected.description)
    assert_that(actual.type).is_equal_to(expected.type)


@parameterized.expand([
    ('text', 'text'),
    ('    ', ''),
    ('abc\nabc', 'abc\nabc'),
    (':class:`CatCat`: it means Cat', ':class:`CatCat`: it means Cat')
])
def test_parse_docstring_only_text(text, expected_text):
    parsed = parse_docstring(text)
    assert_that(parsed.text).is_equal_to(expected_text)
    assert_that(parsed.returns).is_none()
    assert_that(parsed.args).is_empty()


def test_parse_param1():
    parsed = parse_docstring('''Does nothing
    
    But wait... It turns <br> tags into newlines
    
    :param k: k is k, it's simple!)
    :type k: int
    :param \*: its a star, no type
        but has an indented :ref:`sentence`''')

    k, star = parsed.args
    assert_that(parsed.returns).is_none()
    assert_equals(k, FunctionArg('k', 'int', 'k is k, it\'s simple!)'))
    assert_equals(star, FunctionArg('\*', '', 'its a star, no type\n    but has an indented :ref:`sentence`'))

    assert_that(parsed.text).is_equal_to('Does nothing\n\nBut wait... It turns <br> tags into newlines\n')