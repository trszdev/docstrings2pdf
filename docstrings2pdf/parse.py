from docstrings2pdf.typing import *
from ast import parse as _parse, FunctionDef, ClassDef, AST, get_docstring
from typing import Union, Tuple
from os.path import basename
from astunparse import unparse


def unparse_strip(node):
    return unparse(node).strip()


def parse(source: bytes, filename: str = None) -> SourceFile:
    try:
        ast = _parse(source, filename)
        children = tuple(filter(bool, map(_ast2sn, ast.body)))  # type: Tuple[SourceNode]
        result = SourceFile(children, filename)
        return result
    except SyntaxError as e:
        raise ParseError(e)


def _arg2str(x):
    (arg, anno), default = x
    result = [arg]
    if anno is not None:
        result.append(f': {anno.id}')
    if default is not None:
        result.append(f'={unparse_strip(default)}')
    return ''.join(result)


def _ast2sn(ast: AST) -> Union[None, SourceNode]:
    snt = SourceNodeType.FUNCTION
    args = []
    if isinstance(ast, ClassDef):
        snt = SourceNodeType.CLASS
        args = [*map(unparse_strip, ast.bases)]
    elif isinstance(ast, FunctionDef):
        a = ast.args
        pos_default = [None] * (len(a.args) - len(a.defaults)) + a.defaults
        args.extend(map(_arg2str, zip(((x.arg, x.annotation) for x in a.args), pos_default)))
        if a.kwonlyargs:
            args.append('*')
            args.extend(map(_arg2str, zip(((x.arg, x.annotation) for x in a.kwonlyargs), a.kw_defaults)))
        if a.vararg is not None:
            args.append(f'*{a.vararg.arg}')
        if a.kwarg is not None:
            args.append(f'**{a.kwarg.arg}')
        if ast.returns is not None:
            args.append(f'-> {ast.returns.id}')
    else:
        return None
    doc = get_docstring(ast)
    children = tuple(filter(bool, map(_ast2sn, ast.body)))  # type: Tuple[SourceNode]
    return SourceNode(snt, children, ast.name, doc, tuple(args))


def parse_file(filename: str) -> SourceFile:
    with open(filename, 'rb') as handle:
        return parse(handle.read(), basename(filename))
