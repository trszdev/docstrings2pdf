from pytest import main
from os import chdir, getcwd
from os.path import join

chdir(join(getcwd(), 'docstrings2pdf', 'tests'))
main(['-c', '../../pytest.ini', '--pyargs', 'docstrings2pdf.tests'])